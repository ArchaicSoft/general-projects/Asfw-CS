﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Asfw
{
    /// <summary>
    /// A buffer object made from a byte array.
    /// </summary>
    public struct ByteStream : IDisposable
    {
        /// <summary>
        /// The internal byte array.
        /// </summary>
        public byte[] Data;

        /// <summary>
        /// The read/write head. This moves on read AND write code so keep track of this
        /// if attempting to read and write at the same time.
        /// </summary>
        public int Head;

        #region General

        /// <summary>
        /// Initializes with a preset size to optimize write speeds by reducing
        /// resize times.
        /// </summary>
        public ByteStream(int initialSize = 4)
        {
            if (initialSize < 1) initialSize = 4;
            Data = new byte[initialSize];
            Head = 0;
        }

        /// <summary>
        /// Initializes using data which can be read.
        /// </summary>
        /// <param name="bytes"></param>
        public ByteStream(byte[] bytes)
        {
            Data = bytes;
            Head = 0;
        }

        /// <summary>
        /// Clears internal data. Only required if memory usage is
        /// high and the GC is not collecting fast enough.
        /// </summary>
        public void Dispose()
        {
            Data = null;
            Head = 0;
        }

        /// <summary>
        /// Returns data after cutting off unused array indexes.
        /// </summary>
        public byte[] ToArray()
        {
            var ret = new byte[Head];
            Buffer.BlockCopy(Data, 0, ret, 0, Head);
            return ret;
        }

        /// <summary>
        /// For use with the networking of this library.
        /// Packs the data array with an extra 4 bytes at the
        /// beginning expressing the Head value.
        /// </summary>
        public byte[] ToPacket()
        {
            var ret = new byte[4 + Head];
            var size = BitConverter.GetBytes(Head);
            Buffer.BlockCopy(size, 0, ret, 0, 4);
            Buffer.BlockCopy(Data, 0, ret, 4, Head);
            return ret;
        }

        private void CheckSize(int length)
        {
            var len = Data.Length;
            if (length + Head < len) return;
            if (len < 4) len = 4;
            len *= 2;

            while (length + Head >= len) len *= 2;

            var buf = new byte[len];
            Buffer.BlockCopy(Data, 0, buf, 0, Head);
            Data = buf;
        }

        #endregion

        #region Read

        /// <summary>
        /// Reads a byte array from the stream without reading for a prewritten size.
        /// </summary>
        public byte[] ReadBlock(int size)
        {
            if (size <= 0 || Head + size > Data.Length) return new byte[0];

            var ret = new byte[size];
            Buffer.BlockCopy(Data, Head, ret, 0, size);
            Head += size;
            return ret;
        }

        /// <summary>
        /// Reads a serializable object from the stream.
        /// </summary>
        public object ReadObject()
        {
            var value = ReadBytes();
            if (value.Length <= 0) return null;

            using (var ms = new MemoryStream(value))
            {
                var bf = new BinaryFormatter {Binder = new AsfwBinder()};
                return bf.Deserialize(ms);
            }
        }

        /// <summary>
        /// Reads a byte array and its size from the stream.
        /// </summary>
        public byte[] ReadBytes()
        {
            if (Head + 4 > Data.Length) return new byte[0];

            var len = BitConverter.ToInt32(Data, Head);
            Head += 4;
            if (len <= 0 || Head + len > Data.Length) return new byte[0];

            var ret = new byte[len];
            Buffer.BlockCopy(Data, Head, ret, 0, len);
            Head += len;
            return ret;
        }

        /// <summary>
        /// Reads a regular string from the stream.
        /// </summary>
        public string ReadString()
        {
            if (Head + 4 > Data.Length) return "";

            var len = BitConverter.ToInt32(Data, Head);
            Head += 4;
            if (len <= 0 || Head + len > Data.Length) return "";

            var ret = Encoding.UTF8.GetString(Data, Head, len);
            Head += len;
            return ret;
        }

        /// <summary>
        /// Reads a (character) from the stream.
        /// </summary>
        public char ReadChar()
        {
            if (Head + 2 > Data.Length) return char.MinValue;

            var ret = BitConverter.ToChar(Data, Head);
            Head += 2;
            return ret;
        }

        /// <summary>
        /// Reads a (byte) from the stream.
        /// </summary>
        public byte ReadByte()
        {
            if (Head + 1 > Data.Length) return 0;

            var ret = Data[Head];
            Head += 1;
            return ret;
        }

        /// <summary>
        /// Reads a (boolean) from the stream.
        /// </summary>
        public bool ReadBoolean()
        {
            if (Head + 1 > Data.Length) return false;

            var ret = BitConverter.ToBoolean(Data, Head);
            Head += 1;
            return ret;
        }

        /// <summary>
        /// Reads a (short) from the stream.
        /// </summary>
        public short ReadInt16()
        {
            if (Head + 2 > Data.Length) return 0;

            var ret = BitConverter.ToInt16(Data, Head);
            Head += 2;
            return ret;
        }

        /// <summary>
        /// Reads a (unsigned short) from the stream.
        /// </summary>
        public ushort ReadUInt16()
        {
            if (Head + 2 > Data.Length) return 0;

            var ret = BitConverter.ToUInt16(Data, Head);
            Head += 2;
            return ret;
        }

        /// <summary>
        /// Reads a (integer) from the stream.
        /// </summary>
        public int ReadInt32()
        {
            if (Head + 4 > Data.Length) return 0;

            var ret = BitConverter.ToInt32(Data, Head);
            Head += 4;
            return ret;
        }

        /// <summary>
        /// Reads a (usigned integer) from the stream.
        /// </summary>
        public uint ReadUInt32()
        {
            if (Head + 4 > Data.Length) return 0;

            var ret = BitConverter.ToUInt32(Data, Head);
            Head += 4;
            return ret;
        }

        /// <summary>
        /// Reads a (single-floating point) from the stream.
        /// </summary>
        public float ReadSingle()
        {
            if (Head + 4 > Data.Length) return 0.0f;

            var ret = BitConverter.ToSingle(Data, Head);
            Head += 4;
            return ret;
        }

        /// <summary>
        /// Reads a (long) from the stream.
        /// </summary>
        public long ReadInt64()
        {
            if (Head + 8 > Data.Length) return 0;

            var ret = BitConverter.ToInt64(Data, Head);
            Head += 8;
            return ret;
        }

        /// <summary>
        /// Reads a (unsigned long) from the stream.
        /// </summary>
        public ulong ReadUInt64()
        {
            if (Head + 8 > Data.Length) return 0;

            var ret = BitConverter.ToUInt64(Data, Head);
            Head += 8;
            return ret;
        }

        /// <summary>
        /// Reads a (double-floating point) from the stream.
        /// </summary>
        public double ReadDouble()
        {
            if (Head + 8 > Data.Length) return 0.0;

            var ret = BitConverter.ToDouble(Data, Head);
            Head += 8;
            return ret;
        }

        #endregion

        #region Write

        /// <summary>
        /// Writes a byte array to the stream without prewriting its size.
        /// </summary>
        public void WriteBlock(byte[] bytes)
        {
            CheckSize(bytes.Length);
            Buffer.BlockCopy(bytes, 0, Data, Head, bytes.Length);
            Head += bytes.Length;
        }

        /// <summary>
        /// Writes a byte array to the stream without prewriting its size.
        /// </summary>
        public void WriteBlock(byte[] bytes, int offset, int size)
        {
            CheckSize(size);
            Buffer.BlockCopy(bytes, offset, Data, Head, size);
            Head += size;
        }

        /// <summary>
        /// Writes a serializable object to the stream.
        /// </summary>
        public void WriteObject(object value)
        {
            using (var ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, value);
                WriteBytes(ms.GetBuffer());
            }
        }

        /// <summary>
        /// Writes a byte array and its size to the stream.
        /// </summary>
        public void WriteBytes(byte[] value, int offset, int size)
        {
            WriteBlock(BitConverter.GetBytes(size));
            WriteBlock(value, offset, size);
        }

        /// <summary>
        /// Writes a byte array and its size to the stream.
        /// </summary>
        public void WriteBytes(byte[] value)
        {
            WriteBlock(BitConverter.GetBytes(value.Length));
            WriteBlock(value);
        }

        /// <summary>
        /// Writes a regular string to the stream.
        /// </summary>
        public void WriteString(string value)
        {
            if (value is null)
            {
                WriteBlock(BitConverter.GetBytes(0));
                return;
            }

            var bytes = Encoding.UTF8.GetBytes(value);
            WriteBlock(BitConverter.GetBytes(bytes.Length));
            WriteBlock(bytes);
        }

        /// <summary>
        /// Writes a (character) to the stream.
        /// </summary>
        public void WriteChar(char value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (byte) to the stream.
        /// </summary>
        public void WriteByte(byte value)
        {
            CheckSize(1);
            Data[Head] = value;
            Head += 1;
        }

        /// <summary>
        /// Writes a (boolean) to the stream.
        /// </summary>
        public void WriteBoolean(bool value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (short) to the stream.
        /// </summary>
        public void WriteInt16(short value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (unsigned short) to the stream.
        /// </summary>
        public void WriteUInt16(ushort value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (integer) to the stream.
        /// </summary>
        public void WriteInt32(int value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (unsigned integer) to the stream.
        /// </summary>
        public void WriteUInt32(uint value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (single-floating point) to the stream.
        /// </summary>
        public void WriteSingle(float value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (long) to the stream.
        /// </summary>
        public void WriteInt64(long value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (unsigned long) to the stream.
        /// </summary>
        public void WriteUInt64(ulong value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a (double-floating point) to the stream.
        /// </summary>
        public void WriteDouble(double value)
        {
            WriteBlock(BitConverter.GetBytes(value));
        }

        #endregion

        /// <summary> Internal use binder. </summary>
        private class AsfwBinder : SerializationBinder
        {
            public override Type BindToType(string assemblyName, string typeName)
            {
                if (assemblyName is null) throw new ArgumentNullException(nameof(assemblyName));
                assemblyName = System.Reflection.Assembly.GetEntryAssembly()?.FullName;
                return Type.GetType(typeName + ", " + assemblyName);
            }
        }
    }
}