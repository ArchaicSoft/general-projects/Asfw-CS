﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Asfw.Network
{
    public sealed class Client : IDisposable
    {
        private Socket _socket;
        private byte[] _receiveBuffer;
        private byte[] _packetRing;
        private int _packetCount;
        private int _packetSize;
        private bool _connecting;

        /// <summary>
        /// If this value is true then packets will not be parsed to function pointers automatically
        /// but will still receive data asynchronously. The result would require you to run the
        /// ParsePackets() function to handle the packets on the thread that needs to handle them.
        /// </summary>
        public bool ThreadControl { get; set; } = false;

        public delegate void ConnectionArgs();

        public delegate void DataArgs(ref byte[] data);

        public delegate void CrashReportArgs(string reason);

        public delegate void PacketInfoArgs(int size, int header, ref byte[] data);

        public delegate void TrafficInfoArgs(int size, ref byte[] data);

        public event ConnectionArgs ConnectionSuccess;
        public event ConnectionArgs ConnectionFailed;
        public event ConnectionArgs ConnectionLost;
        public event CrashReportArgs CrashReport;
        public event PacketInfoArgs PacketReceived;
        public event TrafficInfoArgs TrafficReceived;
        public DataArgs[] PacketId;

        /// <summary> Initialize with packet settings. </summary>
        /// <param name="packetCount">Constant size of max packet header count.</param>
        public Client(int packetCount, int packetSize = 8192)
        {
            if (!(_socket is null)) return;
            if (packetSize <= 0) packetSize = 8192;

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _packetCount = packetCount;
            _packetSize = packetSize;
            PacketId = new DataArgs[packetCount];
        }

        /// <summary> Closes and cleans up network data. </summary>
        public void Dispose()
        {
            if (_socket is null) return;

            Disconnect();
            _socket.Close();
            _socket.Dispose();
            _socket = null;
            PacketId = null;
            ConnectionSuccess = null;
            ConnectionFailed = null;
            ConnectionLost = null;
            CrashReport = null;
            PacketReceived = null;
            TrafficReceived = null;
            PacketId = null;
        }

        /// <summary>
        /// Connects to the specified remote end point.
        /// Invokes an event if connection was successful or failed.
        /// </summary>
        public void Connect(string ip, int port)
        {
            if (_socket is null || _socket.Connected || _connecting) return;

            if (ip.ToLower() == "localhost")
            {
                _socket.BeginConnect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), port), DoConnect, null);
                return;
            }

            _connecting = true;
            _socket.BeginConnect(new IPEndPoint(IPAddress.Parse(ip), port), DoConnect, null);
        }

        private void DoConnect(IAsyncResult ar)
        {
            if (_socket is null) return;

            try
            {
                _socket.EndConnect(ar);
            }
            catch
            {
                ConnectionFailed?.Invoke();
                _connecting = false;
                return;
            }

            if (!_socket.Connected)
            {
                ConnectionFailed?.Invoke();
                _connecting = false;
                return;
            }

            _connecting = false;
            ConnectionSuccess?.Invoke();
            _socket.ReceiveBufferSize = _packetSize;
            _socket.SendBufferSize = _packetSize;

            if (!ThreadControl) BeginReceiveData();
        }

        /// <summary>
        /// Returns true if a connection exists. May return false positive if connection was
        /// terminated incorrectly. (EX: Other side crashed and never requestd disconnect)
        /// </summary>
        public bool IsConnected => !(_socket is null) && _socket.Connected;

        /// <summary>
        /// Signals termination of a connection to the other side if connected to anyone.
        /// </summary>
        public void Disconnect()
        {
            if (_socket is null || !_socket.Connected) return;
            _socket.BeginDisconnect(false, DoDisconnect, null);
        }

        private void DoDisconnect(IAsyncResult ar)
        {
            if (_socket is null) return;
            try
            {
                _socket.EndDisconnect(ar);
            }
            catch
            {
                /* Ignore error */
            }

            ConnectionLost?.Invoke();
        }

        private void BeginReceiveData()
        {
            _receiveBuffer = new byte[_packetSize];
            _socket.BeginReceive(_receiveBuffer, 0, _packetSize, SocketFlags.None, DoReceive, null);
        }

        private void DoReceive(IAsyncResult ar)
        {
            if (_socket is null) return;

            var size = 0;
            try
            {
                size = _socket.EndReceive(ar);
            }
            catch
            {
                CrashReport?.Invoke("ConnectionForciblyClosedException");
                Disconnect();
                return;
            }

            if (size < 1)
            {
                if (_socket is null) return;
                CrashReport?.Invoke("BufferUnderflowException");
                Disconnect();
                return;
            }

            TrafficReceived?.Invoke(size, ref _receiveBuffer);

            if (_packetRing is null)
            {
                _packetRing = new byte[size];
                Buffer.BlockCopy(_receiveBuffer, 0, _packetRing, 0, size);
            }
            else
            {
                var len = _packetRing.Length;
                var data = new byte[len + size];
                Buffer.BlockCopy(_packetRing, 0, data, 0, len);
                Buffer.BlockCopy(_receiveBuffer, 0, data, len, size);
                _packetRing = data;
            }

            PacketHandler();

            _receiveBuffer = new byte[_packetSize];
            _socket.BeginReceive(_receiveBuffer, 0, _packetSize, SocketFlags.None, DoReceive, null);
        }

        private void PacketHandler()
        {
            var len = _packetRing.Length;
            var pos = 0;
            var count = 0;
            var size = 0;
            var index = 0;
            byte[] data;

            while (true)
            {
                count = len - pos;
                if (count >= 4)
                {
                    size = BitConverter.ToInt32(_packetRing, pos);
                    if (size >= 4)
                    {
                        if (size <= count)
                        {
                            pos += 4;
                            index = BitConverter.ToInt32(_packetRing, pos);

                            if (index >= 0 && index < _packetCount)
                            {
                                if (!(PacketId[index] is null))
                                {
                                    var pSize = size - 4;
                                    data = new byte[pSize];

                                    if (pSize > 0)
                                        Buffer.BlockCopy(_packetRing,
                                            pos + 4, data, 0, pSize);

                                    PacketReceived?.Invoke(pSize, index, ref data);
                                    PacketId[index](ref data);
                                    pos += size;
                                }
                                else
                                {
                                    CrashReport?.Invoke("NullReferenceException");
                                    Disconnect();
                                    return;
                                }
                            }
                            else
                            {
                                CrashReport?.Invoke("IndexOutOfRangeException");
                                Disconnect();
                                return;
                            }
                        }
                        else break;
                    }
                    else
                    {
                        CrashReport?.Invoke("BrokenPacketException");
                        Disconnect();
                        return;
                    }
                }
                else break;
            }

            if (count == 0)
            {
                _packetRing = null;
            }
            else
            {
                var buffer = new byte[count];
                Buffer.BlockCopy(_packetRing, pos, buffer, 0, count);
                _packetRing = buffer;
            }
        }

        /// <summary>
        /// This is a utility function to control which thread actually
        /// invokes the packet handlers. Only usable if ThreadControl is true.
        /// </summary>
        public void ReceiveData()
        {
            if (!ThreadControl) return;
            _receiveBuffer = new byte[_packetSize];
            _socket.ReceiveTimeout = 1;

            try
            {
                SocketError err;
                var size = _socket.Receive(_receiveBuffer, 0, _packetSize, SocketFlags.None, out err);
                if (err == SocketError.TimedOut) return;
                if (err != SocketError.Success) throw new Exception($"Receive error: {err}");
                if (size < 1) return;

                TrafficReceived?.Invoke(size, ref _receiveBuffer);

                if (_packetRing is null)
                {
                    _packetRing = new byte[size];
                    Buffer.BlockCopy(_receiveBuffer, 0, _packetRing, 0, size);
                }
                else
                {
                    var len = _packetRing.Length;
                    var data = new byte[len + size];
                    Buffer.BlockCopy(_packetRing, 0, data, 0, len);
                    Buffer.BlockCopy(_receiveBuffer, 0, data, len, size);
                    _packetRing = data;
                }

                PacketHandler();

                _receiveBuffer = new byte[_packetSize];
            }
            catch (Exception e)
            {
                throw new Exception($"Something went wrong with receiving a packet! Err:[{e}]");
            }
        }

        /// <summary>
        /// Sends byte array data over the network to the connected endpoint.
        /// [Used internally, but can be used externally if creating your own
        /// SendToAll/But to reduce overhead if ref data has been optimized already.]
        /// 
        /// Note: Only use ByteStream.ToPacket() with this method.
        /// </summary>
        public void SendData(byte[] data)
        {
            if (!_socket.Connected) return;
            _socket?.BeginSend(data, 0, data.Length, SocketFlags.None, DoSend, null);
        }

        /// <summary>
        /// Sends byte array data over the network to the connected endpoint.
        /// </summary>
        public void SendData(byte[] data, int head)
        {
            if (!_socket.Connected) return;
            var buffer = new byte[head + 4];
            Buffer.BlockCopy(BitConverter.GetBytes(head), 0, buffer, 0, 4);
            Buffer.BlockCopy(data, 0, buffer, 4, head);
            _socket?.BeginSend(buffer, 0, head + 4, SocketFlags.None, DoSend, null);
        }

        private void DoSend(IAsyncResult ar)
        {
            try
            {
                _socket.EndSend(ar);
            }
            catch
            {
                CrashReport?.Invoke("ConnectionForciblyClosedException");
                Disconnect();
            }
        }
    }
}