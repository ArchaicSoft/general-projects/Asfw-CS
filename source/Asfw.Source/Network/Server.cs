using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Asfw.Network
{
    public sealed class Server : IDisposable
    {
        private struct ReceiveState : IDisposable
        {
            internal readonly int Index;
            internal int PacketCount;
            internal byte[] Buffer;
            internal byte[] RingBuffer;

            internal ReceiveState(int index, int packetSize)
            {
                Index = index;
                PacketCount = 0;
                Buffer = new byte[packetSize];
                RingBuffer = null;
            }

            public void Dispose()
            {
                Buffer = null;
                RingBuffer = null;
            }
        }

        public Dictionary<int, Socket> _socket;
        public List<int> _unsignedIndex;
        private Socket _listener;
        private IAsyncResult _pendingAccept;
        private int _packetCount;
        private int _packetSize;

        /// <summary>
        /// Limitation of packet sizes to allow being received. 0 Meens no limit anything is received.
        /// </summary>
        public int BufferLimit { get; set; } = 0;

        /// <summary>
        /// Returns the client limit set on initialization.
        /// </summary>
        public int ClientLimit { get; }

        /// <summary>
        /// Returns true if listener is active and can receive connection requests.
        /// </summary>
        public bool IsListening { get; private set; }

        /// <summary>
        /// Returns the highest index used by active clients.
        /// </summary>
        public int HighIndex { get; private set; }

        /// <summary>
        /// Sets the minimum index the listener starts from.
        /// </summary>
        public int MinimumIndex { get; set; } = 0;

        /// <summary>
        /// Returns a list of the active index values currently used for easier iteration.
        /// </summary>
        public List<int> ConnectionIds()
        {
            if (_socket is null) return new List<int>();
            return new List<int>(_socket.Keys);
        }

        /// <summary>
        /// The limit to how many packets can be stored on the buffer at one time.
        /// 0 for no limit.
        /// </summary>
        public int PacketAcceptLimit { get; set; } = 0;

        /// <summary>
        /// If expected packets should never reach this number (EX - A connection is spamming)
        /// then automatically disconnect. Basicly this is something like a DDOS prevention tool.
        /// 0 to never disconnect.
        /// </summary>
        public int PacketDisconnectCount { get; set; } = 0;

        public delegate void AccessArgs(int index, int packet_id);

        public delegate void ConnectionArgs(int index);

        public delegate void DataArgs(int index, ref byte[] data);

        public delegate void CrashReportArgs(int index, string reason);

        public delegate void PacketInfoArgs(int size, int header, ref byte[] data);

        public delegate void TrafficInfoArgs(int size, ref byte[] data);

        public delegate void NullArgs();

        public event AccessArgs AccessCheck;
        public event ConnectionArgs ConnectionReceived;
        public event ConnectionArgs ConnectionLost;
        public event CrashReportArgs CrashReport;
        public event PacketInfoArgs PacketReceived;
        public event TrafficInfoArgs TrafficReceived;
        public DataArgs[] PacketId;

        /// <summary> Initialize with packet settings. </summary>
        /// <param name="packetCount">Constant size of max packet header count.</param>
        /// <param name="packetSize">Constant size of packet buffer size.</param>
        /// <param name="clientLimit">If left at 0 new connections will never be refused.</param>
        public Server(int packetCount, int packetSize = 8192, int clientLimit = 0)
        {
            if (!(_listener is null) || !(_socket is null)) return;
            if (packetSize <= 0) packetSize = 8192;

            _socket = new Dictionary<int, Socket>();
            _unsignedIndex = new List<int>();
            ClientLimit = clientLimit;
            _packetCount = packetCount;
            _packetSize = packetSize;
            PacketId = new DataArgs[packetCount];
        }

        /// <summary> Closes and cleans up network data. </summary>
        public void Dispose()
        {
            if (_socket is null) return;

            StopListening();

            if (_socket.Count > 0)
                foreach (var index in _socket.Keys)
                    Disconnect(index);

            _socket.Clear();
            _socket = null;
            PacketId = null;
            _unsignedIndex.Clear();
            _unsignedIndex = null;
            AccessCheck = null;
            ConnectionReceived = null;
            ConnectionLost = null;
            CrashReport = null;
            PacketReceived = null;
            TrafficReceived = null;
            PacketId = null;
        }

        /// <summary>
        /// Returns true if a connection exists. May return false positive if connection was
        /// terminated incorrectly. (EX: Other side crashed and never requestd disconnect)
        /// </summary>
        public bool IsConnected(int index)
        {
            if (_socket is null) return false;
            if (!_socket.ContainsKey(index)) return false;
            if (_socket[index].Connected) return true;
            Disconnect(index);
            return false;
        }

        /// <summary>
        /// Returns the Local Area Net connection IP or IPv4 of the server host computer.
        /// </summary>
        public string GetIPv4() => Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();

        /// <summary>
        /// Returns the ip saved in the existing 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string ClientIp(int index)
        {
            return IsConnected(index) ? ((IPEndPoint) _socket[index].RemoteEndPoint).Address.ToString() : "[Null]";
        }

        /// <summary>
        /// Signals termination of a connection to the other side if connected to anyone.
        /// </summary>
        public void Disconnect(int index)
        {
            if (_socket is null || !_socket.ContainsKey(index)) return;

            var socket = _socket[index];
            if (socket is null)
            {
                _socket.Remove(index);
                _unsignedIndex.Add(index);
                return;
            }

            socket.BeginDisconnect(false, DoDisconnect, index);
        }

        private void DoDisconnect(IAsyncResult ar)
        {
            if (_socket is null) return;

            var index = (int) ar.AsyncState;
            try
            {
                _socket[index].EndDisconnect(ar);
            }
            catch
            {
                /* Ignore error */
            }

            if (!_socket.ContainsKey(index)) return; // Bugged?

            _socket[index].Dispose();
            _socket[index] = null;
            _socket.Remove(index);
            _unsignedIndex.Add(index);

            var i = HighIndex;
            while(!_socket.ContainsKey(i))
            {
                if (i == MinimumIndex) break;
                i--;
            }
            HighIndex = i;

            ConnectionLost?.Invoke(index);

            // Re-invoke listener that was temporarily disabled by reaching limit
            if (ClientLimit > 0 && ClientLimit <= _socket.Count) ListenManager();
        }

        private int FindEmptySlot(int startIndex)
        {
            foreach (var index in _unsignedIndex)
            {
                if (HighIndex < index) HighIndex = index;
                
                _unsignedIndex.Remove(index);
                return index;
            }

            if (_socket.Count == 0)
            {
                HighIndex = startIndex;
                return startIndex;
            }
            else
            {
                HighIndex += 1;
                return HighIndex;
            }
        }

        /// <summary>
        /// Activates listener if it is not already active.
        /// </summary>
        public void StartListening(int port, int backlog)
        {
            if (_socket is null || IsListening || !(_listener is null)) return;

            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _listener.Bind(new IPEndPoint(IPAddress.Any, port));
            IsListening = true;
            _listener.Listen(backlog);

            ListenManager();
        }

        /// <summary>
        /// Deactivates listener if it is active.
        /// </summary>
        public void StopListening()
        {
            if (!IsListening || _socket is null) return;

            IsListening = false;
            if (_listener is null) return;

            _listener.Close();
            _listener.Dispose();
            _listener = null;
        }

        private void DoAcceptClient(IAsyncResult ar)
        {
            try
            {
                var socket = EndAccept(ar);
                if (!(socket is null))
                {
                    var index = FindEmptySlot(MinimumIndex);
                    _socket.Add(index, socket);
                    _socket[index].ReceiveBufferSize = _packetSize;
                    _socket[index].SendBufferSize = _packetSize;
                    BeginReceiveData(index);
                    ConnectionReceived?.Invoke(index);
                }
            }
            catch
            {
                // Something borked, gtfo -> who cares what the issue was
                ListenManager();
                return;
            }

            ListenManager();
        }

        private Socket EndAccept(IAsyncResult ar = null)
        {
            var pendingAccept = ar ?? _pendingAccept;
            if (pendingAccept is null || _listener is null)
            {
                return null;
            }

            _pendingAccept = null;
            return _listener.EndAccept(pendingAccept);
        }

        private void ListenManager()
        {
            if (!IsListening || _listener is null || !(_pendingAccept is null)) return;
            if (ClientLimit > 0 && ClientLimit <= _socket.Count) return;
            _pendingAccept = _listener.BeginAccept(DoAcceptClient, null);
        }

        private void BeginReceiveData(int index)
        {
            var so = new ReceiveState(index, _packetSize);
            try
            {
                _socket[index].BeginReceive(so.Buffer, 0,
                    _packetSize, SocketFlags.None, DoReceive, so);
            }
            catch
            {
                /* Do Nothing */
            }
        }

        private void DoReceive(IAsyncResult ar)
        {
            if (_socket is null) return;

            var so = (ReceiveState) ar.AsyncState;
            var size = 0;

            try
            {
                size = _socket[so.Index].EndReceive(ar);
            }
            catch
            {
                CrashReport?.Invoke(so.Index, "ConnectionForciblyClosedException");
                Disconnect(so.Index);
                so.Dispose();
                return;
            }

            if (size < 1)
            {
                if (!_socket.ContainsKey(so.Index))
                {
                    so.Dispose();
                    return;
                }

                if (_socket[so.Index] is null)
                {
                    so.Dispose();
                    return;
                }

                CrashReport?.Invoke(so.Index, "BufferUnderflowException");
                Disconnect(so.Index);
                so.Dispose();
                return;
            }

            TrafficReceived?.Invoke(size, ref so.Buffer);
            so.PacketCount++;

            if (PacketDisconnectCount > 0 && so.PacketCount >= PacketDisconnectCount)
            {
                CrashReport?.Invoke(so.Index, "Packet Spamming/DDOS");
                Disconnect(so.Index);
                so.Dispose();
                return;
            }

            if (PacketAcceptLimit == 0 || PacketAcceptLimit > so.PacketCount)
            {
                if (so.RingBuffer is null)
                {
                    so.RingBuffer = new byte[size];
                    Buffer.BlockCopy(so.Buffer, 0, so.RingBuffer, 0, size);
                }
                else
                {
                    var len = so.RingBuffer.Length;
                    var data = new byte[len + size];
                    Buffer.BlockCopy(so.RingBuffer, 0, data, 0, len);
                    Buffer.BlockCopy(so.Buffer, 0, data, len, size);
                    so.RingBuffer = data;
                }

                if (BufferLimit > 0 && so.RingBuffer.Length > BufferLimit)
                {
                    Disconnect(so.Index);
                    so.Dispose();
                    return;
                }
            }

            if (!_socket.ContainsKey(so.Index))
            {
                so.Dispose();
                return;
            }

            if (_socket[so.Index] is null || !_socket[so.Index].Connected)
            {
                Disconnect(so.Index);
                so.Dispose();
                return;
            }

            PacketHandler(ref so);
            so.Buffer = new byte[_packetSize];
            if (!_socket.ContainsKey(so.Index))
            {
                so.Dispose();
                return;
            }

            try
            {
                _socket[so.Index].BeginReceive(so.Buffer, 0, _packetSize, SocketFlags.None,
                    DoReceive, so);
            }
            catch
            {
                /* Do Nothing */
            }
        }

        private void PacketHandler(ref ReceiveState so)
        {
            var connectionID = so.Index;
            var len = so.RingBuffer.Length;
            var pos = 0;
            var count = 0;
            var size = 0;
            var index = 0;
            var didHandle = false;
            byte[] data;

            while (true)
            {
                count = len - pos;

                if (count >= 4)
                {
                    size = BitConverter.ToInt32(so.RingBuffer, pos);

                    if (size >= 4)
                    {
                        if (size <= count)
                        {
                            pos += 4;
                            index = BitConverter.ToInt32(so.RingBuffer, pos);

                            if (index >= 0 && index < _packetCount)
                            {
                                if (!(PacketId[index] is null))
                                {
                                    if (!(AccessCheck is null))
                                    {
                                        AccessCheck.Invoke(connectionID, index);
                                        if (!_socket.ContainsKey(connectionID))
                                        {
                                            so.Dispose();
                                            return;
                                        }
                                    }

                                    var pSize = size - 4;
                                    data = new byte[pSize];

                                    if (pSize > 0)
                                        Buffer.BlockCopy(so.RingBuffer,
                                            pos + 4, data, 0, pSize);

                                    PacketReceived?.Invoke(pSize, index, ref data);
                                    PacketId[index](connectionID, ref data);

                                    pos += size;
                                    so.PacketCount--;
                                    didHandle = true;
                                }
                                else
                                {
                                    if (!_socket.ContainsKey(connectionID))
                                    {
                                        so.Dispose();
                                        return;
                                    }

                                    CrashReport?.Invoke(connectionID, "NullReferenceException");
                                    Disconnect(connectionID);
                                    so.Dispose();
                                    return;
                                }
                            }
                            else
                            {
                                if (!_socket.ContainsKey(connectionID))
                                {
                                    so.Dispose();
                                    return;
                                }

                                CrashReport?.Invoke(connectionID, "IndexOutOfRangeException");
                                Disconnect(connectionID);
                                so.Dispose();
                                return;
                            }
                        }
                        else break;
                    }
                    else
                    {
                        if (!_socket.ContainsKey(connectionID))
                        {
                            so.Dispose();
                            return;
                        }

                        CrashReport?.Invoke(connectionID, "BrokenPacketException");
                        Disconnect(connectionID);
                        so.Dispose();
                        return;
                    }
                }
                else break;
            }

            if (count == 0)
            {
                so.RingBuffer = null;
                so.PacketCount = 0;
            }
            else
            {
                var buffer = new byte[count];
                Buffer.BlockCopy(so.RingBuffer, pos, buffer, 0, count);
                so.RingBuffer = buffer;

                if (didHandle) so.PacketCount = 1;
            }
        }

        /// <summary>
        /// Sends byte array data over the network to the connected endpoint.
        /// [Used internally, but can be used externally if creating your own
        /// SendToAll/But to reduce overhead if ref data has been optimized already.]
        /// 
        /// Note: Only use ByteStream.ToPacket() with this method.
        /// </summary>
        public void SendDataTo(int index, byte[] data)
        {
            if (!_socket.ContainsKey(index)) return;
            if (_socket[index] is null || !_socket[index].Connected)
            {
                Disconnect(index);
                return;
            }

            _socket[index].BeginSend(data, 0, data.Length, SocketFlags.None, DoSend, index);
        }

        /// <summary>
        /// Sends byte array data over the network to the connected endpoint.
        /// </summary>
        public void SendDataTo(int index, byte[] data, int head)
        {
            if (!_socket.ContainsKey(index)) return;
            if (_socket[index] is null || !_socket[index].Connected)
            {
                Disconnect(index);
                return;
            }

            var buffer = new byte[head + 4];
            Buffer.BlockCopy(BitConverter.GetBytes(head), 0, buffer, 0, 4);
            Buffer.BlockCopy(data, 0, buffer, 4, head);
            _socket[index].BeginSend(buffer, 0, head + 4, SocketFlags.None, DoSend, index);
        }

        /// <summary>
        /// Sends byte array data over the network to all connected endpoints.
        /// 
        /// Note: Only use ByteStream.ToPacket() with this method.
        /// </summary>
        public void SendDataToAll(byte[] data)
        {
            for (var i = 0; i <= HighIndex; i++)
                if (_socket.ContainsKey(i))
                    SendDataTo(i, data);
        }

        /// <summary>
        /// Sends byte array data over the network to all connected endpoints.
        /// </summary>
        public void SendDataToAll(byte[] data, int head)
        {
            var buffer = new byte[head + 4];
            Buffer.BlockCopy(BitConverter.GetBytes(head), 0, buffer, 0, 4);
            Buffer.BlockCopy(data, 0, buffer, 4, head);

            for (var i = 0; i <= HighIndex; i++)
                if (_socket.ContainsKey(i))
                    SendDataTo(i, buffer);
        }

        /// <summary>
        /// Sends byte array data over the network to all connected endpoints
        /// excluding the specified index.
        /// 
        /// Note: Only use ByteStream.ToPacket() with this method.
        /// </summary>
        public void SendDataToAllBut(int index, byte[] data)
        {
            for (var i = 0; i <= HighIndex; i++)
                if (_socket.ContainsKey(i))
                    if (i != index)
                        SendDataTo(i, data);
        }

        /// <summary>
        /// Sends byte array data over the network to all connected endpoints
        /// excluding the specified index.
        /// </summary>
        public void SendDataToAllBut(int index, byte[] data, int head)
        {
            var buffer = new byte[head + 4];
            Buffer.BlockCopy(BitConverter.GetBytes(head), 0, buffer, 0, 4);
            Buffer.BlockCopy(data, 0, buffer, 4, head);

            for (var i = 0; i <= HighIndex; i++)
                if (_socket.ContainsKey(i))
                    if (i != index)
                        SendDataTo(i, buffer);
        }

        private void DoSend(IAsyncResult ar)
        {
            var index = (int) ar.AsyncState;

            try
            {
                _socket[index].EndSend(ar);
            }
            catch
            {
                CrashReport?.Invoke(index, "ConnectionForciblyClosedException");
                Disconnect(index);
            }
        }
    }
}