﻿using System.Reflection;

[assembly: AssemblyTitle("ASFW")]
[assembly: AssemblyDescription("ArchaicSoft Framework")]
[assembly: AssemblyCompany("ArchaicSoft")]
[assembly: AssemblyProduct("ASFW")]
[assembly: AssemblyCopyright("Copyright © 2016-2020 ArchaicSoft")]
[assembly: AssemblyVersion("0.10.0.0")]
[assembly: AssemblyFileVersion("0.10.0.0")]