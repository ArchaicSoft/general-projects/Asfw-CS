﻿using System;
using Asfw.Network;

namespace client
{
    internal static partial class Network
    {
        internal static Client Socket;

        internal static void InitNetwork()
        {
            // In the event you called InitNetwork more than once in an app.
            if (Socket != null) return;

            // Always place the count of the opposite network object type of packet. This sets up
            // a pointer to pass packets to an appropriate handler WAY faster than a 
            // Select Case Or Giant If/Else  block!
            Socket = new Client((int)ServerPackets.Count);
            Socket.ConnectionSuccess += Socket_ConnectionSuccess;
            Socket.ConnectionLost += Socket_ConnectionLost;
            Socket.ConnectionFailed += Socket_ConnectionFailed;
            Socket.CrashReport += Socket_CrashReport;

            // Always initialize the router after network has been initialized.
            PacketRouter();
        }

        internal static void DestroyNetwork()
        {
            // Calling a disconnect is not necessary when using destroy network as
            // Socket.EndNetwork already calls it and cleans up the memory internally.
            // This is basicly the network objects "Dispose" method.
            Socket.Dispose();
        }

        internal static void Connect(string ip, int port)
        {
            // Ip can be 127.0.0.1 or localhost for the callback ip.
            Socket.Connect(ip, port);
        }

        internal static void Disconnect()
        {
            Socket.Disconnect();
        }

        internal static bool IsConnected()
        {
            // This can leave a false positive if there was a successful
            // connection but was terminated from loss of internet or the
            // server performing an incorrect disconnect of the client.
            return Socket.IsConnected;
        }

        #region Events
        internal static void Socket_ConnectionSuccess()
        {
            // Add code you want to happen whenever connecting to the server
            // completes without issues. Commonly you would setup some first requests
            // or in a game tell a game screen connection was successful and move to character
            // setup or something.

            // Example:
            Console.WriteLine("Now Connected!");
        }

        internal static void Socket_ConnectionFailed()
        {
            // Add code you want to happen whenever connecting to the server
            // did not complete. Either server was offline or there was an error
            // of some kind like maybe port was incorrect or the server just outright
            // refused the connection.

            // Example:
            Console.WriteLine("Connection Failed, Try again!");
        }

        internal static void Socket_ConnectionLost()
        {
            // Add code you want to happen whenever you were ALREADY connected
            // to the server but something happened and you lost the connection.
            // This could happen because the client disconnected improperly
            // without using the disconnect method, internet was lost, the server
            // force booted the client or the network crashed in another various way.

            // Example:
            Console.WriteLine("Connection was lost!");
            DestroyNetwork();
        }

        internal static void Socket_CrashReport(string err)
        {
            // Add code you want to happen whenever a connection crashed. This and connectionlost
            // may both simultaniously activate but not all lost connections could be caused by a crash
            // so this function just allows for reporting what caused a lost connection if due to a network
            // crash of any kind to give clues as to what happened and how you may fix it. Do note:
            // Crash reports will give a general error name such as SocketError or IndexOutOfRange so they may
            // be a bit vague at times!

            // Example:
            Console.WriteLine("There was a network error -> Report: " + err);
        }
        #endregion
    }
}
