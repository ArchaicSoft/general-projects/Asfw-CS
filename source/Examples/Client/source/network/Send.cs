﻿using Asfw;

namespace client
{
    internal static partial class Network
    {
        internal static void SendFakeLogin(string username, string password)
        {
            username = Program.EKeyPair.EncryptString(username);
            password = Program.EKeyPair.EncryptString(password);

            var buffer = new ByteStream(12 + username.Length + password.Length);
            buffer.WriteInt32((int)ClientPackets.FakeLogin);
            buffer.WriteString(username);
            buffer.WriteString(password);

            Socket.SendData(buffer.Data, buffer.Head);

            buffer.Dispose();
        }

        internal static void SendMessage(string msg)
        {
            // Sending packets will always require a buffer.
            // Start with a new bytestream object, and either
            // leave it empty parameters for a default size of 4
            // or give it a presize. Presizing can be useful as
            // when adding data the buffer has to resize itself
            // alot in order to add more data than was initially
            // planned for and resizing if done many times can be
            // very resource heavy and slow. Presizing isnt required,
            // just recommended if possible.

            // # To account for a presize, always concider the following:
            // ##
            // # Byte = 1, Short = 2, Integer, Uinteger, and Single = 4
            // # Long, Ulong and Double = 8. String = 4 + string length.
            // ##
            // # ClientPacket and Server packet are headers for our packet
            // # and must always be written first. They always = 4.
            // ##
            // # In this packet we will presize for 4(PacketHeader) +
            // # 4(storing msg//s length is an integer) + msg.length(for each
            // # character that is in the msg string)
            // ##
            // So we come up with 8 + msg.length! We will not have any internal
            // resizing being done meening maximum efficiency.
            var buffer = new ByteStream(8 + msg.Length);

            buffer.WriteInt32((int)ClientPackets.Message); // The packet header
            buffer.WriteString(msg); // WriteString Automatically writes the length as well as the string!

            Socket.SendData(buffer.Data, buffer.Head);

            // Cleanup
            buffer.Dispose();
        }
    }
}
