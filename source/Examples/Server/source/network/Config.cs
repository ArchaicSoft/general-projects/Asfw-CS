﻿using System;
using Asfw.Network;

namespace server
{
    internal static partial class Network
    {
        internal static Server Socket;

        internal static void InitNetwork()
        {
            // In the event you called InitNetwork more than once in an app.
            if (Socket != null) return;

            // Always place the count of the opposite network object type of packet. This sets up
            // a pointer to pass packets to an appropriate handler WAY faster than a 
            // Select Case Or Giant If/Else  block!

            // The 0 represents a player limit. 0 = no limit while any number >0 is the set limit.
            Socket = new Server((int)ClientPackets.Count, 0, 0);
            Socket.ConnectionReceived += Socket_ConnectionReceived;
            Socket.ConnectionLost += Socket_ConnectionLost;
            Socket.CrashReport += Socket_CrashReport;

            // This is a limit on the max amount of data a single socket can store.
            // Basicly normal packets will be massively smaller than the number
            // preset here, however should the network be slow on either end the socket
            // can store extra packets on a buffer to be handled whenever the server finally
            // received the data. This can normally cause memory leaks if someone intentionally
            // made a massive buffer to eat up all the servers ram so having
            // a buffer limit prevents a socket from storing more than a set capacity
            // to keep things running smooth for everyone.
            //Socket.BufferLimit = 8192; // <- this is 8 kb

            // Always initialize the router after network has been initialized.
            PacketRouter();
        }

        internal static void DestroyNetwork()
        {
            // Calling a disconnect is not necessary when using destroy network as
            // Socket.EndNetwork already calls it and cleans up the memory internally.
            // This is basicly the network objects "Dispose" method.
            Socket.Dispose();
        }

        internal static string GetIPv4()
        {
            // This returns your local area net ip which you would
            // be listed as under your own router. Can also be found
            // in command prompt via the ipconfig command however this
            // isnt as simple when running on linux or mac to find so
            // this function exists to simplify those operating systems.
            return Socket.GetIPv4();
        }

        internal static string ClientIP(int index)
        {
            // This may return either an error or an empty string if
            // the client index was terminated already or the index
            // was out of bounds of the internal socket array(Value unused).
            // This is however safe to call in connectionlost event as the socket
            // isnt destroyed until after the event has been called (Meening the ip
            // still exists in the empty socket object until connectionlost is over).
            return Socket.ClientIp(index);
        }

        internal static int HighIndex()
        {
            // Returns the highest used index in the internal sockets.
            // Its useful to know what they are in relation to a client connection
            // as when tying them to a database such as a player array they should be
            // the same. This can also be useful for interacting with such a database
            // as since they should be the same you can loop through only USED array values
            // instead of the full cap of the array length.
            return Socket.HighIndex;
        }

        internal static int ClientLimit()
        {
            // This will simply report the limit set inside the initializer above.
            return Socket.ClientLimit;
        }

        internal static void Disconnect(int index)
        {
            // This will safely disconnect a client and report
            // to them that the network was closed without error.
            Socket.Disconnect(index);
        }

        internal static bool IsConnected(int index)
        {
            // This can leave a false positive if there was a successful
            // connection but was terminated from loss of internet or the
            // client performing an incorrect disconnect of the client.
            return Socket.IsConnected(index);
        }

        #region Listener
        internal static void StartListening(int port, int backlog)
        {
            // Backlog can be used for when multiple clients try to connect all at
            // the same time then it will allow a temporary connection of clients
            // up to the backlog number and refuse the rest of the connection attempts
            // until the connection has been assigned a socket, index, and been handled
            // by the ConnectionReceived event. AKA: if 10 people try to connect at 
            // close to the same time and backlog = 5 then the last 5 people will
            // be directly refused. This is totally necessary to help protect against
            // DDOS attacks as a ddosser could make thousands of connection attempts
            // very quickly and memory leak the server.
            Socket.StartListening(port, backlog);
        }

        internal static bool IsListening()
        {
            // If any problems occur, the server never started listening, or the server
            // had already stopped listening before the intended end of the program,
            // this function will return false. Otherwise the server will return true
            // when StartListening has been called and no problems have occured to interupt.
            // Listening is required in order to accept connection requests => Aka the client.
            return Socket.IsListening;
        }

        internal static void StopListening()
        {
            Socket.StopListening();
        }
        #endregion

        #region Events
        internal static void Socket_ConnectionReceived(int index)
        {
            // Add code you want to happen whenever a connection request was accepted as a client.
            // When connection is accepted it has already been assigned an index, you may want to initialize
            // a Player object here with database information about a character or some other things tied to an account.

            // Example:
            Console.WriteLine("Connection received on index[" + index + "] - IP[" + ClientIP(index) + "]");
            SendKeyPair(index);
        }

        internal static void Socket_ConnectionLost(int index)
        {
            // Add code you want to happen whenever an existing client connection was terminated.
            // This may be from client disconnecting themselves, the client being disconnected by the server,
            // or maybe the client was lost due to network error / crash.
            // Most people would use this to cleanup database objects tied to a player here.

            // Example:
            Console.WriteLine("Connection lost on index[" + index + "] - IP[" + ClientIP(index) + "]");
        }

        internal static void Socket_CrashReport(int index, string err)
        {
            // Add code you want to happen whenever a connection crashed. This and connectionlost
            // may both simultaniously activate but not all lost connections could be caused by a crash
            // so this function just allows for reporting what caused a lost connection if due to a network
            // crash of any kind to give clues as to what happened and how you may fix it. Do note:
            // Crash reports will give a general error name such as SocketError or IndexOutOfRange so they may
            // be a bit vague at times!

            // Example:
            Console.WriteLine("There was a network error -> Index[" + index + "]");
            Console.WriteLine("Report: " + err);
        }

#if (DEBUG)
        internal static void Socket_PacketReceieved()
        {
            // This function is in a DEBUG compiler command as this command itself
            // is only needed in debugging mode when you might be testing traffic.
            // as quickly as this event may be called because every packet even if its not
            // complete can activate this its simply a solid chunk of data is received from ANY
            // connection and this event is called. This can be very slow on the application
            // and really should only be used for testing how many packets you are receiving
            // which can be useful to tell you if measured correctly you may have a memory leak,
            // a spammer, or a hacker tampering with the network so you can take steps to figure
            // out how to remove any of the listed problems above.

            // Example:
            Console.WriteLine("Packet Invoke");
        }
#endif
        #endregion
    }
}
